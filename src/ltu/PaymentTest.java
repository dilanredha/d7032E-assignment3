package ltu;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import java.util.Calendar;
import java.util.Date;


public class PaymentTest
{
    
    private CalendarImpl calendar = null;
    private CalendarSpring calendarSpring = null;
    private CalendarSaturday calendarSaturday = null;
    private PaymentImpl paymentImpl = null;
    private PaymentImpl paymentImplTest = null;
    private PaymentImpl paymentImplSpring = null;
    private PaymentImpl paymentImplSaturday = null;

    

    @Before
    public void init() throws IOException { 
        calendar = new CalendarImpl();
        calendarSpring = new CalendarSpring();
        calendarSaturday = new CalendarSaturday();
        paymentImpl = new PaymentImpl(calendar);
        paymentImplSpring = new PaymentImpl(calendarSpring);
        paymentImplSaturday = new PaymentImpl(calendarSaturday);
    }

    @Test(expected = Exception.class)
    public void paymnetTest() throws IOException{
        paymentImplTest = new PaymentImpl(null, null);

    }

    @Test(expected = Exception.class)
    public void personIdNull() throws IllegalArgumentException {
        paymentImpl.getMonthlyAmount(null, 0, 100, 100);
    }

    @Test(expected = Exception.class)
    public void personIdLessThan13() throws IllegalArgumentException {
        paymentImpl.getMonthlyAmount("19982312", 0, 100, 100);
    }

    @Test(expected = Exception.class)
    public void personIncomeNegative() throws IllegalArgumentException {
        paymentImpl.getMonthlyAmount("19980626-0817", -1, 100, 100);
    }

    @Test(expected = Exception.class)
    public void personStudyRateNegative() throws IllegalArgumentException {
        paymentImpl.getMonthlyAmount("19980626-0817", 0, -1, 100);
    }

    @Test(expected = Exception.class)
    public void personCompleteNegative() throws IllegalArgumentException {
        paymentImpl.getMonthlyAmount("19980626-0817", 0, 100, -1);
    }

    @Test
    public void test101() {
        assertEquals(9904, paymentImpl.getMonthlyAmount("19900221-1374", 0, 100, 100));
        assertEquals(0, paymentImpl.getMonthlyAmount("20030606-1374", 0, 100, 100));
    }

    @Test 
    public void test102() {
        assertEquals(2816, paymentImpl.getMonthlyAmount("19720221-1374", 0, 100, 100)); 
    }
    
    @Test
    public void test102ReallyOldGuy() {
        assertEquals(0, paymentImpl.getMonthlyAmount("19651221-1374", 0, 100, 100));   
    }

    @Test
    public void test103() {
        assertEquals(9904, paymentImpl.getMonthlyAmount("19760101-1374", 0, 100, 100));
    }
    @Test
    public void test103SemiOldGuy() {
        assertEquals(2816, paymentImpl.getMonthlyAmount("19750101-1374", 0, 100, 100));
    }

    @Test
    public void test201() {
        assertEquals(4960, paymentImpl.getMonthlyAmount("19980626-0817", 0, 50, 100));
    }
    @Test
    public void test201SlowStudyRate() {
        assertEquals(0, paymentImpl.getMonthlyAmount("19980626-0817", 0, 49, 100));
    }

    @Test
    public void test202() {
        assertEquals(4960, paymentImpl.getMonthlyAmount("19980626-0817", 0, 99, 100));
    }
    
    @Test
    public void test203() {
        assertEquals(9904, paymentImpl.getMonthlyAmount("19980626-0817", 0, 100, 100));
    }

    @Test
    public void test301() {
        assertEquals(9904, paymentImpl.getMonthlyAmount("19980626-0817", 85813, 100, 100));
        assertEquals(0, paymentImpl.getMonthlyAmount("19980626-0817", 85814, 100, 100));
    }

    @Test
    public void test302Below() {
        assertEquals(4960, paymentImpl.getMonthlyAmount("19980626-0817", 128722, 50, 100));
    }

    @Test
    public void test302Above() {
        assertEquals(0, paymentImpl.getMonthlyAmount("19980626-0817", 128723, 50, 100));
    }
    
    @Test
    public void test401HalftimeComplete() {
        assertEquals(4960, paymentImpl.getMonthlyAmount("19980626-0817", 0, 50, 50));
    }

    @Test
    public void test401HalftimeFailure() {
        assertEquals(0, paymentImpl.getMonthlyAmount("19980626-0817", 0, 50, 49));
    }

    @Test
    public void test401FulltimeComplete() {
        assertEquals(9904, paymentImpl.getMonthlyAmount("19980626-0817", 0, 100, 50));
    }

    @Test
    public void test401FulltimeFailure() {
        assertEquals(0, paymentImpl.getMonthlyAmount("19980626-0817", 0, 100, 49));
    }

    @Test
    public void test505() {
        assertEquals(9904, paymentImplSpring.getMonthlyAmount("19900626-0817", 0, 100, 100));
        assertEquals(0, paymentImplSpring.getMonthlyAmount("19980626-0817", 0, 100, 100));
    }

    @Test
    public void test506jan() {
        Calendar cal = new Calendar();
        cal.set(2016, Calendar.JANUARY, 01);
        assertEquals("20160129", paymentImplSpring.getNextPaymentDay());
    
    }
    
    @Test
    public void test506feb(){
        assertEquals("20160226", paymentImplSpring.getNextPaymentDay());
    }
    
    @Test
    public void test506mar(){
        assertEquals("20160331", paymentImplSpring.getNextPaymentDay());
    }
    
    @Test
    public void test506apr(){
        assertEquals("20160429", paymentImplSpring.getNextPaymentDay());
        
    }
    @Test
    public void test506may(){
        assertEquals("20160531", paymentImplSpring.getNextPaymentDay());
        
    }
    @Test
    public void test506jun(){
        assertEquals("20160630", paymentImplSpring.getNextPaymentDay());
        
    }
    
    @Test
    public void test506OtherDate() {
        // System.out.println(paymentImplSaturday.getNextPaymentDay());
        assertEquals("20220429", paymentImplSaturday.getNextPaymentDay());
        
    }

}